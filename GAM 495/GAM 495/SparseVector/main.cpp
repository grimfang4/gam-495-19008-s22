#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class SparseVector {
public :

	SparseVector()
	{

	}

	SparseVector(vector<float>& vector)
	{
		//map int to float
		//two vectors?
		//unordered map
		for (int i = 0; i < vector.size(); i++)
		{
			if (vector[i] != 0)
				//map.insert(make_pair(i, vector[i]));
				map.emplace(i, vector[i]);
		}
	}

	static float dotProduct(const SparseVector &A, const SparseVector &B)
	{
		float answer = 0;
		for (auto p : A.map)
		{
			auto e = B.map.find(p.first);
			if (e != B.map.end())
			{
				answer += e->second * p.second;
			}
			
		}

		return answer;
	}

	void Clear()
	{
		map.clear();
	}

	int Size() const
	{
		return map.size();
	}

	unordered_map<int, float> map;
};


int main(int argc, char* argv[])
{
	//two empties
	SparseVector::dotProduct(SparseVector(), SparseVector());
}