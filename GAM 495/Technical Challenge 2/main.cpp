#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;

/*
1) Given an array filled with 1s and 0s, sort it with a single for-loop.
*/

void Fn1(int* array, int size)
{
	for (int left = 0, right = size - 1; left < right; )
	{
		if (array[right] == 1)
		{
			right--;
			continue;
		}
		if (array[left] == 0)
		{
			left++;
			continue;
		}
		swap(left, right);
		left++;
		right--;
	}
}


/*
2) Given a list of unsorted integers, return the k'th smallest.
One Solution and a Constraint: One immediate approach is to sort the list and then return the k'th element. Can you do it faster in the average case?
*/

template<typename T>
class MinHeap
{
public:
	void Insert(const T& value)
	{
		// Add to the end
		data.push_back(value);
		BubbleUp();
	}

	void Remove()
	{
		if (Size() == 0)
			throw;
		if (Size() == 1)
		{
			data.clear();
		}
		else
		{
			// Get rid of the first element
			// replace it with the last element
			auto e = data.begin() + (data.size() - 1);
			data[0] = *e;
			data.erase(e);

			BubbleDown();
		}
	}

	T& Peek()
	{
		if(data.size() == 0)
			throw;
		return data[0];
	}

	int Size()
	{
		return data.size();
	}

private:

	vector<T> data;

	void BubbleUp()
	{
		int i = data.size() - 1;

		// {0, 1, 2, 3, 4, 5, 6, 7}

		
		for (int parent = (i - 1) / 2; parent >= 0; parent = (i - 1) / 2)
		{
			if (data[i] < data[parent])
			{
				// Swap and follow upward
				swap(data[i], data[parent]);
				i = parent;
			}
			else
				break;
		}
	}

	void BubbleDown()
	{
		int i = 0;

		while (true)
		{
			int left = i * 2 + 1;
			int right = left + 1;

			int swapCandidate = i;

			if (left < data.size() && data[left] < data[i])
				swapCandidate = left;// potential swap
			if (right < data.size() && data[right] < data[swapCandidate])
				swapCandidate = right;// potential swap

			if (swapCandidate != i)
			{
				swap(data[i], data[swapCandidate]);
				i = swapCandidate;
			}
			else
				break;
		}
	}
};



int main(int argc, char* argv[])
{
	MinHeap<int> myHeap;

	for (int i = 0; i < 20; ++i)
	{
		int n = rand() % 100;
		myHeap.Insert(n);
		cout << n << " ";
	}
	cout << endl;

	while (myHeap.Size() > 0)
	{
		cout << myHeap.Peek() << " ";
		myHeap.Remove();
	}

	return 0;
}